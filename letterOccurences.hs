{-# LANGUAGE ScopedTypeVariables #-}

import System.IO  
import System.Environment
import Control.Monad
import qualified Data.Text.Lazy as L
import qualified Data.Text.Lazy.IO as LIO
import qualified Data.Text.Lazy.Encoding as LE
import qualified Data.Map as M
import Data.Char


type CharCountMap = M.Map Char Int


normalizedCharMap = M.fromList [
        ('á', 'a'), ('à', 'a'), ('ä', 'a'), ('â', 'a'), ('ã', 'a'),
        ('é', 'e'), ('è', 'e'), ('ê', 'e'), ('ë', 'e'),
        ('í', 'i'), ('î', 'i'), ('ï', 'i'), ('í', 'i'),
        ('ó', 'o'), ('ô', 'o'), ('ò', 'o'), ('õ', 'o'), ('ö', 'o'),
        ('ú', 'u'), ('û', 'u'), ('ü', 'u'), ('ù', 'u'),
        ('ý', 'y'), ('ÿ', 'y'),
        ('ç', 'c')
    ]


normalizeCh :: Char -> Char
normalizeCh c =
    if isAlpha lc
        then
            case M.lookup lc normalizedCharMap of
            Nothing -> lc
            Just ascii -> ascii
        else
            '\0'
    where
        lc = toLower c


updateCountMap :: Char -> CharCountMap -> CharCountMap
updateCountMap ch countMapAcc =
    if normalizedCh /= '\0'
        then
            case M.lookup normalizedCh countMapAcc of
                Nothing ->
                    M.insert normalizedCh 1 countMapAcc
                Just count ->
                    M.update (\x -> Just (count + 1)) normalizedCh countMapAcc
        else
            countMapAcc

    where
        normalizedCh = normalizeCh ch


createCountMap :: L.Text -> CharCountMap
createCountMap t =
    L.foldr updateCountMap M.empty t


createCountList :: CharCountMap -> [(Char, Int)]
createCountList countMap = 
        map (\(w, c) -> (w, c)) baseCountList
    where
        baseCountList = M.toAscList countMap


computeCharacterCountStr :: L.Text -> IO [Char]
computeCharacterCountStr contents = do
    let countList = createCountList $ createCountMap contents
    let finalCountList = map (\(_, c) -> c) countList

    let finalListBase = foldl (\acc c -> acc ++ (show c)) "" finalCountList

    --putStrLn $ show countList
    --putStrLn $ show finalCountList
    return finalListBase


main = do  
    args <- getArgs

    case args of
        [] ->
            putStrLn "letterOccurences [file]"

        (file:xs) -> do
            handle <- openFile (head args) ReadMode
            contents :: L.Text <- LIO.hGetContents handle

            putStrLn $ "Output for file " ++ (head args) ++ " is: "
            finalListBase <- computeCharacterCountStr contents
            putStrLn $ "finalListBase: " ++ finalListBase

            hClose handle  
            return ()

